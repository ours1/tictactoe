TIC_TAC_TOE_BOARD = [0, 1, 2, 3, 4, 5, 6, 7, 8]


def print_board():
    print("---------------")  # Use these as visual aid
    print("Current board:")
    # Let's print slices
    print(TIC_TAC_TOE_BOARD[0:3])  # Print first row
    print(TIC_TAC_TOE_BOARD[3:6])  # Print second row
    print(TIC_TAC_TOE_BOARD[6:])  # Print third row
    print("---------------")


# Function to move player
def player_move(player_mark):
    # ask the player where to move
    player_position = input(f"Where do you want to put your '{player_mark}'? ")

    # validating the number as is between 0 and 8
    if (player_position == "0" or player_position == "1" or
        player_position == "2" or player_position == "3" or
        player_position == "4" or player_position == "5" or
        player_position == "6" or player_position == "7" or
            player_position == "8"):

        # need to make the answer to int, because we use indexes
        player_position = int(player_position)
        # validate if the position is not taken
        if TIC_TAC_TOE_BOARD[player_position] == "X" or TIC_TAC_TOE_BOARD[player_position] == "O":
            print("You can't choose place that has been taken!")
            player_move(player_mark)  # calling the function again
        else:
            # remove the inserted value
            TIC_TAC_TOE_BOARD.pop(player_position)
            # insert 'X' to the removed place
            TIC_TAC_TOE_BOARD.insert(player_position, player_mark)
    else:
        print("You have to insert number from 0 to 8!")
        player_move(player_mark)  # calling the function again


def winner(player_mark):
    if ((TIC_TAC_TOE_BOARD[0] == player_mark and TIC_TAC_TOE_BOARD[1] == player_mark and TIC_TAC_TOE_BOARD[2] == player_mark) or
        (TIC_TAC_TOE_BOARD[3] == player_mark and TIC_TAC_TOE_BOARD[4] == player_mark and TIC_TAC_TOE_BOARD[5] == player_mark) or
        (TIC_TAC_TOE_BOARD[6] == player_mark and TIC_TAC_TOE_BOARD[7] == player_mark and TIC_TAC_TOE_BOARD[8] == player_mark) or
        (TIC_TAC_TOE_BOARD[0] == player_mark and TIC_TAC_TOE_BOARD[3] == player_mark and TIC_TAC_TOE_BOARD[6] == player_mark) or
        (TIC_TAC_TOE_BOARD[1] == player_mark and TIC_TAC_TOE_BOARD[4] == player_mark and TIC_TAC_TOE_BOARD[7] == player_mark) or
        (TIC_TAC_TOE_BOARD[2] == player_mark and TIC_TAC_TOE_BOARD[5] == player_mark and TIC_TAC_TOE_BOARD[8] == player_mark) or
        (TIC_TAC_TOE_BOARD[0] == player_mark and TIC_TAC_TOE_BOARD[4] == player_mark and TIC_TAC_TOE_BOARD[8] == player_mark) or
            (TIC_TAC_TOE_BOARD[2] == player_mark and TIC_TAC_TOE_BOARD[4] == player_mark and TIC_TAC_TOE_BOARD[6] == player_mark)):
        print(f"{player_mark} have won!")
        return True
    else:
        return False


def play_again():
    again = input("Do you want to play again? y/n ")
    # takes indexes in use from list, because numbers are gone
    if again == "y":
        # erasing the values
        TIC_TAC_TOE_BOARD.clear()
        # new values
        clear_board = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        # add new values to the board
        TIC_TAC_TOE_BOARD.extend(clear_board)
        gameplay()
    elif again == "n":
        print("Good bye")
    else:
        print("You have to insert 'y' or 'n'")
        play_again()


def gameplay():
    print_board()
    print("Player one is 'X' and player two is 'O'.")
    print("Player one starts.")
    i = 0  # defining i for while loop
    while i < 9:
        player_move(player_mark="X")
        print_board()
        if winner(player_mark="X") is True:
            break
        i += 1
        if i == 9:
            print("It's a tie!")
            break
        player_move(player_mark="O")
        print_board()
        if winner(player_mark="O") is True:
            break
        i += 1
    play_again()

# call the game
gameplay()
